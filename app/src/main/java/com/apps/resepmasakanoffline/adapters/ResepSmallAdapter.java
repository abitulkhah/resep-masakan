package com.apps.resepmasakanoffline.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.activities.BookmarkActivity;
import com.apps.resepmasakanoffline.activities.DetailResepActivity;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.apps.resepmasakanoffline.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ResepSmallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ResepItem> resepList;
    private List<ResepItem> resepBookmarks;

    private static final int AD_TYPE = 0;

    // The unified native ad view type.
    private static final int CONTENT_TYPE = 1;

    private InputStream inputstream;
    private Drawable drawable;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView resepTitle, resepCategory, resepViewsCount;
        public ImageView resepImage;
        public ImageButton resepBookmarkSmall;
        public RelativeLayout smallListRL;

        public MyViewHolder(View view) {
            super(view);
            resepTitle = (TextView) view.findViewById(R.id.resepTitleSmall);
            resepCategory = (TextView) view.findViewById(R.id.resepCategorySmall);
            resepImage = (ImageView) view.findViewById(R.id.resepImageSmall);
            resepBookmarkSmall = (ImageButton) view.findViewById(R.id.resepBookmarkSmall);
            smallListRL = (RelativeLayout) view.findViewById(R.id.smallListRL);
        }
    }


    public ResepSmallAdapter(Context mContext, List<ResepItem> resepList) {
        this.mContext = mContext;
        this.resepList = resepList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;

        if (viewType == AD_TYPE) {

            view = new AdView(mContext);
            ((AdView) view).setAdSize(AdSize.SMART_BANNER);
            ((AdView) view).setAdUnitId("ca-app-pub-5430928142469598/8569294975");
            ((AdView) view).loadAd(new AdRequest.Builder().build());
            ((AdView) view).setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    System.out.println("on onAdLoaded");
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // Code to be executed when an ad request fails.
                    System.out.println("on onAdFailedToLoad");
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                    System.out.println("on onAdOpened");
                }

                @Override
                public void onAdClicked() {
                    // Code to be executed when the user clicks on an ad.
                    System.out.println("on onAdClicked");
                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                    System.out.println("on onAdLeftApplication");
                }

                @Override
                public void onAdClosed() {
                    // Code to be executed when the user is about to return
                    // to the app after tapping on an ad.
                    System.out.println("on onAdClosed");
                }
            });
//            ((AdView) view).loadAd(new AdRequest.Builder().addTestDevice("91C315C6B594A47AB3861549EFCA31F4").build());
        }else{

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.resep_small_card, parent, false);

        }
        return new ResepSmallAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        switch (viewType) {

            case AD_TYPE:

                break;

            default:

                final MyViewHolder myViewHolder = (MyViewHolder) holder;
                final ResepItem resepItem = (ResepItem) resepList.get(position);

                myViewHolder.resepTitle.setText(resepItem.getTitle());
                myViewHolder.resepCategory.setText(resepItem.getCategory().getCategoryName());

                if(mContext instanceof BookmarkActivity){
                    myViewHolder.resepBookmarkSmall.setVisibility(View.GONE);
                }

                LoadData loadData = new LoadData();
                loadData.loadImage(mContext, myViewHolder.resepImage, resepItem.getImage());

                myViewHolder.smallListRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mContext, DetailResepActivity.class);
                        intent.putExtra("resepId", resepItem.getId());
                        intent.putExtra("resepTitle", resepItem.getTitle());
                        intent.putExtra("resepCategory", resepItem.getCategory().toString());
                        intent.putExtra("description", resepItem.getDescription());
                        intent.putExtra("image", resepItem.getImage());
                        intent.putExtra("categoryName", resepItem.getCategory().getCategoryName());
                        intent.putExtra("url", resepItem.getUrl());

                        mContext.startActivity(intent);
                    }
                });

                myViewHolder.resepBookmarkSmall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sharedPreferences = mContext.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
                        String bookmarkData = sharedPreferences.getString("bookmarkList", "");
                        boolean existData = false;

                        Gson gson = new Gson();

                        Type type = new TypeToken<List<ResepItem>>() {}.getType();

                        if(bookmarkData.equalsIgnoreCase("") || bookmarkData.equalsIgnoreCase(null)){
                            resepBookmarks = new ArrayList<ResepItem>();
                        }else {
                            resepBookmarks = gson.fromJson(bookmarkData, type);
                            for(ResepItem resepCheck : resepBookmarks){
                                if(resepCheck.getId() == resepItem.getId()){
                                    existData = true;
                                }
                            }
                        }


                        if(!existData){
                            resepBookmarks.add(resepItem);
                            String json = gson.toJson(resepBookmarks);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("bookmarkList", json);
                            editor.commit();
                        }

                        Toast.makeText(mContext, "Menambahkan Ke Bookmark", Toast.LENGTH_SHORT).show();


                    }
                });
        }


    }

    @Override
    public int getItemCount() {
        return resepList.size();
    }


    @Override
    public int getItemViewType(int position) {

        if (position > 0 && position % 4 == 0)
            return AD_TYPE;
        return CONTENT_TYPE;
    }
}
