package com.apps.resepmasakanoffline.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.activities.BigResepActivity;
import com.apps.resepmasakanoffline.models.ResepCategoryItem;
import com.apps.resepmasakanoffline.rest.LoadData;

import java.util.List;

public class ResepCategoryAdapter extends RecyclerView.Adapter<ResepCategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<ResepCategoryItem> resepCategoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView resepCategoryTitle;
        public ImageView resepCategoryImage;

        public MyViewHolder(View view) {
            super(view);
            resepCategoryTitle = (TextView) view.findViewById(R.id.resepCategoryTitle);
            resepCategoryImage = (ImageView) view.findViewById(R.id.resepCategoryImage);
        }
    }


    public ResepCategoryAdapter(Context mContext, List<ResepCategoryItem> resepCategoryList) {
        this.mContext = mContext;
        this.resepCategoryList = resepCategoryList;
    }

    @Override
    public ResepCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.resep_category_card, parent, false);

        return new ResepCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ResepCategoryAdapter.MyViewHolder holder, int position) {

        final ResepCategoryItem resepCategoryItem = resepCategoryList.get(position);
        holder.resepCategoryTitle.setText(resepCategoryItem.getCategoryName());
        LoadData loadData = new LoadData();
        loadData.loadImage(mContext, holder.resepCategoryImage, resepCategoryItem.getImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, BigResepActivity.class);
                intent.putExtra("resepCategoryId", resepCategoryItem.getId());
                intent.putExtra("resepCategoryName", resepCategoryItem.getCategoryName());

                mContext.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {
        return resepCategoryList.size();
    }
}
