package com.apps.resepmasakanoffline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.models.SearchResep;
import com.apps.resepmasakanoffline.R;

import java.util.List;

public class SearchResepAdapter extends RecyclerView.Adapter<SearchResepAdapter.MyViewHolder> {

private Context mContext;
private List<SearchResep> mostPopularNewsList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView searchNewsTitle, searchNewsTime;
    public ImageView searchNewsImage;

    public MyViewHolder(View view) {
        super(view);
        searchNewsTitle = (TextView) view.findViewById(R.id.searchNewsTitle);
        searchNewsTime = (TextView) view.findViewById(R.id.searchNewsTime);
        searchNewsImage = (ImageView) view.findViewById(R.id.searchNewsImage);
    }
}


    public SearchResepAdapter(Context mContext, List<SearchResep> mostPopularNewsList) {
        this.mContext = mContext;
        this.mostPopularNewsList = mostPopularNewsList;
    }

    @Override
    public SearchResepAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_resep_card, parent, false);

        return new SearchResepAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SearchResepAdapter.MyViewHolder holder, int position) {
        SearchResep mostPopularNews = mostPopularNewsList.get(position);
        holder.searchNewsTitle.setText(mostPopularNews.getNewsTitle());
        holder.searchNewsTime.setText(mostPopularNews.getNewsTime());
        holder.searchNewsImage.setImageDrawable(mostPopularNews.getNewsImage());
    }


    @Override
    public int getItemCount() {
        return mostPopularNewsList.size();
    }
}

