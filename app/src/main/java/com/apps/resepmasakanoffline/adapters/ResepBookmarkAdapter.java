package com.apps.resepmasakanoffline.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.models.ResepBookmark;

import java.util.List;

public class ResepBookmarkAdapter extends RecyclerView.Adapter<ResepBookmarkAdapter.MyViewHolder> {

    private Context mContext;
    private List<ResepBookmark> resepBookmarkList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bookmarkTitle, bookmarkCategory;
        public ImageView bookmarkImage;

        public MyViewHolder(View view) {
            super(view);
            bookmarkTitle = (TextView) view.findViewById(R.id.bookmarkTitle);
            bookmarkCategory = (TextView) view.findViewById(R.id.bookmarkCategory);
            bookmarkImage = (ImageView) view.findViewById(R.id.bookmarkImage);
        }
    }


    public ResepBookmarkAdapter(Context mContext, List<ResepBookmark> resepBookmarkList) {
        this.mContext = mContext;
        this.resepBookmarkList = resepBookmarkList;
    }

    @Override
    public ResepBookmarkAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.resep_bookmark_card, parent, false);

        return new ResepBookmarkAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ResepBookmarkAdapter.MyViewHolder holder, int position) {
        ResepBookmark resepBookmark = resepBookmarkList.get(position);
        holder.bookmarkTitle.setText(resepBookmark.getResepTitle());
        holder.bookmarkCategory.setText(resepBookmark.getResepCategory());
        holder.bookmarkImage.setImageDrawable(resepBookmark.getResepImage());
    }


    @Override
    public int getItemCount() {
        return resepBookmarkList.size();
    }
}
