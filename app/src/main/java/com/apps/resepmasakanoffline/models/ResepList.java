package com.apps.resepmasakanoffline.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResepList {

    @SerializedName("data")
    private List<ResepItem> items;

    public List<ResepItem> getItems() {
        return items;
    }

    public void setItems(List<ResepItem> items) {
        this.items = items;
    }
}
