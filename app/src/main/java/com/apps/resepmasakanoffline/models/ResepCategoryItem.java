package com.apps.resepmasakanoffline.models;

import com.google.gson.annotations.SerializedName;

public class ResepCategoryItem {


    @SerializedName("id")
    private long id;

    @SerializedName("categoryName")
    private String categoryName;

    @SerializedName("image")
    private String image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
