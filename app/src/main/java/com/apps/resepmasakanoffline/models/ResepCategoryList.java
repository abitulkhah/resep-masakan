package com.apps.resepmasakanoffline.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResepCategoryList {

    @SerializedName("data")
    private List<ResepCategoryItem> items;

    public List<ResepCategoryItem> getItems() {
        return items;
    }

}
