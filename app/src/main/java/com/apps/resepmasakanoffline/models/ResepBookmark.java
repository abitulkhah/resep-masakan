package com.apps.resepmasakanoffline.models;

import android.graphics.drawable.Drawable;

public class ResepBookmark {
    private String resepTitle;
    private String resepCategory;
    private Drawable resepImage;

    public ResepBookmark(String resepTitle, String resepCategory, Drawable resepImage) {
        this.resepTitle = resepTitle;
        this.resepCategory = resepCategory;
        this.resepImage = resepImage;
    }

    public String getResepTitle() {
        return resepTitle;
    }

    public void setResepTitle(String resepTitle) {
        this.resepTitle = resepTitle;
    }

    public String getResepCategory() {
        return resepCategory;
    }

    public void setResepCategory(String resepCategory) {
        this.resepCategory = resepCategory;
    }

    public Drawable getResepImage() {
        return resepImage;
    }

    public void setResepImage(Drawable resepImage) {
        this.resepImage = resepImage;
    }
}
