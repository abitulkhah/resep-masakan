package com.apps.resepmasakanoffline.models;

import android.graphics.drawable.Drawable;

public class SearchResep {
    private String resepTitle;
    private String resepTime;
    private Drawable resepImage;

    public SearchResep(String resepTitle, String resepTime, Drawable resepImage) {
        this.resepTitle = resepTitle;
        this.resepTime = resepTime;
        this.resepImage = resepImage;
    }

    public String getNewsTitle() {
        return resepTitle;
    }

    public void setNewsTitle(String resepTitle) {
        this.resepTitle = resepTitle;
    }

    public String getNewsTime() {
        return resepTime;
    }

    public void setNewsTime(String resepTime) {
        this.resepTime = resepTime;
    }

    public Drawable getNewsImage() {
        return resepImage;
    }

    public void setNewsImage(Drawable resepImage) {
        this.resepImage = resepImage;
    }
}
