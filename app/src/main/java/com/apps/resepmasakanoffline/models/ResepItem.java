package com.apps.resepmasakanoffline.models;

import com.google.gson.annotations.SerializedName;

public class ResepItem {

    @SerializedName("id")
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("url")
    private String url;

    @SerializedName("isLatest")
    private boolean isLatest;

    @SerializedName("isMain")
    private boolean isMain;

    @SerializedName("isPopuler")
    private boolean isPopuler;

    @SerializedName("isSecret")
    private boolean isSecret;

    @SerializedName("isTips")
    private boolean isTips;

    @SerializedName("category")
    private ResepCategoryItem category;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isLatest() {
        return isLatest;
    }

    public void setLatest(boolean latest) {
        isLatest = latest;
    }

    public boolean isMain() {
        return isMain;
    }

    public void setMain(boolean main) {
        isMain = main;
    }

    public boolean isPopuler() {
        return isPopuler;
    }

    public void setPopuler(boolean populer) {
        isPopuler = populer;
    }

    public boolean isSecret() {
        return isSecret;
    }

    public void setSecret(boolean secret) {
        isSecret = secret;
    }

    public boolean isTips() {
        return isTips;
    }

    public void setTips(boolean tips) {
        isTips = tips;
    }

    public ResepCategoryItem getCategory() {
        return category;
    }

    public void setCategory(ResepCategoryItem category) {
        this.category = category;
    }
}
