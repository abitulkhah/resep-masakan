package com.apps.resepmasakanoffline.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class LocaleDateFormat {

    public String showLocaleDate(Date dateTime, String pattern) {

        String dateString = null;
        SimpleDateFormat formatter = null;
        formatter = new SimpleDateFormat(pattern, new Locale("in", "ID"));
        TimeZone tz = TimeZone.getTimeZone("Asia/Jakarta");
        formatter.setTimeZone(tz);
        dateString = formatter.format(dateTime)+" WIB";
        return dateString;

    }
}
