package com.apps.resepmasakanoffline.response;

import com.apps.resepmasakanoffline.models.Resep;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResepResponse {
    @SerializedName("data")
    private List<Resep> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public ResepResponse(List<Resep> data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public List<Resep> getData() {
        return data;
    }

    public void setData(List<Resep> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
