package com.apps.resepmasakanoffline.response;

import com.apps.resepmasakanoffline.models.ResepCategory;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResepCategoryResponse {
    @SerializedName("data")
    private List<ResepCategory> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public ResepCategoryResponse(List<ResepCategory> data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
