package com.apps.resepmasakanoffline.response;

import com.apps.resepmasakanoffline.models.Resep;
import com.google.gson.annotations.SerializedName;

public class ResepDetailResponse {

    @SerializedName("data")
    private Resep data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private Integer status;

    public ResepDetailResponse(Resep data, String message, Integer status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }

    public Resep getData() {
        return data;
    }

    public void setData(Resep data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
