package com.apps.resepmasakanoffline.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.adapters.ResepCategoryAdapter;
import com.apps.resepmasakanoffline.models.ResepCategoryList;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.apps.resepmasakanoffline.utils.AutoColumn;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private ResepCategoryAdapter resepCategoryAdapter;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpCategory;
    Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Kategori");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        mContext = getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_resep_category);
        arrowUpCategory = (FloatingActionButton) view.findViewById(R.id.arrow_up_category);

        arrowUpCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerCategory);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        getData();

        return view;
    }

    public void getData(){

        Gson gson = new Gson();
        LoadData loadData = new LoadData();
        ResepCategoryList resepCategoryList = gson.fromJson(loadData.FromAssets(mContext, "category"), ResepCategoryList.class);
        resepCategoryAdapter = new ResepCategoryAdapter(getContext(), resepCategoryList.getItems());
        resepCategoryAdapter.notifyDataSetChanged();

        Integer countSpan = AutoColumn.calculateNoOfColumns(getContext(), 120);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepCategoryAdapter);
        swipeContainer.setRefreshing(false);

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
