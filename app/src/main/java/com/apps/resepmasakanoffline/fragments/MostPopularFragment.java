package com.apps.resepmasakanoffline.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.adapters.ResepSmallAdapter;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.apps.resepmasakanoffline.models.ResepList;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MostPopularFragment extends Fragment {
    
    private RecyclerView recyclerView;
    private ResepSmallAdapter resepSmallAdapter;
    private List<Object> resepListTemp = new ArrayList<>();
    private List<Object> resepList = new ArrayList<>();
    private int page = -1;
    private int size = 10;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton arrowUpMostPopular;

    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of MenuItems and native ads that populate the RecyclerView.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_most_popular, container, false);

        mContext = getContext();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_small);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerMostPopular);
        arrowUpMostPopular = (FloatingActionButton) view.findViewById(R.id.arrow_up_mostpopular);

        arrowUpMostPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        getData();

//        GetAsyncData getAsyncData = new GetAsyncData();
//        getAsyncData.execute();

        return view;
    }

    private class GetAsyncData extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // doInBackground() adalah tempat kita melakukan proses di thread lain
            getData();
            return null;
        }
    }

    public void getData(){

        page = page+1;
        Gson gson = new Gson();
        LoadData loadData = new LoadData();
        ResepList resepList = gson.fromJson(loadData.FromAssets(mContext, "resep"), ResepList.class);

        List<ResepItem> resepItems = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if(i%4==0){
                resepItems.add(resepList.getItems().get(i));
            }
            Random r = new Random();
            resepItems.add(resepList.getItems().get(r.nextInt(resepList.getItems().size())));        }

        resepSmallAdapter = new ResepSmallAdapter(getContext(), resepItems);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepSmallAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));

        if(page==0){
            recyclerView.addItemDecoration(dividerItemDecoration);
        }

        resepSmallAdapter.notifyDataSetChanged();
        recyclerView.setFocusable(true);
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
