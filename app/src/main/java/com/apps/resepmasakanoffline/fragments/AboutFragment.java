package com.apps.resepmasakanoffline.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.activities.BookmarkActivity;


public class AboutFragment extends Fragment {


    private String countNotification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Tentang");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.profile_action_bar, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        System.out.println(id);
        System.out.println(R.id.bookmarkProfile);
        //noinspection SimplifiableIfStatement
        if (id == R.id.bookmarkProfile) {
            directPage(BookmarkActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    public void directPage(Class activityClass){
        Intent intent = new Intent(getActivity(), activityClass);
        getActivity().startActivity(intent);
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
