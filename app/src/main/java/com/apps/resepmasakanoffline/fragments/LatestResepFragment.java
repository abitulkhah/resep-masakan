package com.apps.resepmasakanoffline.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.activities.DetailResepActivity;
import com.apps.resepmasakanoffline.adapters.ResepSmallAdapter;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.apps.resepmasakanoffline.models.ResepList;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LatestResepFragment extends Fragment {

    private RecyclerView recyclerView;
    private ResepSmallAdapter resepSmallAdapter;
    private CardView resepTopCardView;
    private SwipeRefreshLayout swipeContainer;
    private ImageView resepTopImage;
    private TextView resepTopTitle;
    private TextView resepTopCategory;
    private FloatingActionButton arrowUpLatestResep;

    private InputStream inputstream;
    private Drawable drawable;

    private Integer indexResepTop = -1;
    private int page = 0;
    private int size = 10;
    private int totalItemCount = 0;
    private int previousTotalCount = 10;
    private boolean isLoading = false;
    private ProgressBar progressBar;
    private ScrollView scrollView;
    private View view;
    Context mContext;
    LoadData loadData;
    List<ResepItem> resepItems;
    ResepList resepList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_latest_resep, container, false);

        mContext = getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_small);
        resepTopCardView = (CardView) view.findViewById(R.id.resep_top_card_view);
        progressBar = (ProgressBar) view.findViewById(R.id.item_progress_bar);
        scrollView = (ScrollView) view.findViewById(R.id.scroll_view_latest_resep);

        scrollView.setSmoothScrollingEnabled(true);

        loadData = new LoadData();

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        arrowUpLatestResep = (FloatingActionButton) view.findViewById(R.id.arrow_up_latestresep);

        arrowUpLatestResep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.scrollTo(5,10);
            }
        });


        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerLatestResep);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        resepTopImage = (ImageView) view.findViewById(R.id.resepTopImage);
        resepTopTitle = (TextView) view.findViewById(R.id.resepTopTitle);
        resepTopCategory = (TextView) view.findViewById(R.id.resepTopCategory);

        resepTopCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ResepItem resepTopData = (ResepItem) resepList.getItems().get(indexResepTop);

                Intent intent = new Intent(getActivity(), DetailResepActivity.class);
                intent.putExtra("resepId", resepTopData.getId());
                intent.putExtra("resepId", resepTopData.getId());
                intent.putExtra("resepTitle", resepTopData.getTitle());
                intent.putExtra("resepCategory", resepTopData.getCategory().toString());
                intent.putExtra("description", resepTopData.getDescription());
                intent.putExtra("image", resepTopData.getImage());
                intent.putExtra("categoryName", resepTopData.getCategory().getCategoryName());
                intent.putExtra("url", resepTopData.getUrl());

                getActivity().startActivity(intent);

            }
        });

        getData();

//        GetAsyncData getAsyncData = new GetAsyncData();
//        getAsyncData.execute();

        return view;
    }

    private class GetAsyncData extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // doInBackground() adalah tempat kita melakukan proses di thread lain
            getData();
            return null;
        }
    }

    public void getData(){

        page = page+1;
        Gson gson = new Gson();
        resepList = gson.fromJson(loadData.FromAssets(mContext, "resep"), ResepList.class);

        resepItems = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if(i%4==0){
                resepItems.add(resepList.getItems().get(i));
            }
            Random r = new Random();
            resepItems.add(resepList.getItems().get(r.nextInt(resepList.getItems().size())));
        }
        setTopImage(resepList);
        resepSmallAdapter = new ResepSmallAdapter(mContext, resepItems);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepSmallAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));

        if(page==0){
            recyclerView.addItemDecoration(dividerItemDecoration);
        }

        resepSmallAdapter.notifyDataSetChanged();
        recyclerView.setFocusable(true);
        scrollView.setFocusable(true);
        swipeContainer.setRefreshing(false);
        progressBar.setVisibility(View.GONE);

    }

    public void setTopImage(ResepList data){

        indexResepTop = (int) (Math.random()*135);
        System.out.println(indexResepTop);
        final ResepItem resepItem = (ResepItem) data.getItems().get(indexResepTop);
        loadData.loadImage(mContext, resepTopImage, resepItem.getImage());
        resepTopTitle.setText(resepItem.getTitle());
        resepTopCategory.setText(resepItem.getCategory().getCategoryName());
        resepTopCardView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
