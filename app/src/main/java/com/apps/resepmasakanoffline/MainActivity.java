package com.apps.resepmasakanoffline;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.apps.resepmasakanoffline.fragments.CategoryFragment;
import com.apps.resepmasakanoffline.fragments.HomeFragment;
import com.apps.resepmasakanoffline.fragments.AboutFragment;
import com.apps.resepmasakanoffline.fragments.InfoAndTipsFragment;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private int counter = 0;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        Fragment fragment = null;
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_category:
                    fragment = new CategoryFragment();
                    break;
                case R.id.navigation_video:
                    fragment = new InfoAndTipsFragment();
                    break;
                case R.id.navigation_profile:
                    fragment = new AboutFragment();
                    break;
            }
            return openFragment(fragment);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(R.style.AppTheme);

        setContentView(R.layout.activity_main);

        openFragment(new HomeFragment());
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        registerAdmob();

    }

    public boolean openFragment(Fragment fragment){

        if(fragment!=null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    public void registerAdmob(){
        MobileAds.initialize(this,
                getString(R.string.admob_app_id));
    }


    @Override
    protected void onStart() {
        super.onStart();
        counter = 0;
    }

    @Override
    public void onBackPressed() {
        counter++;
        if(counter == 1) {
            Toast.makeText(MainActivity.this,"Tekan sekali lagi untuk keluar aplikasi", Toast.LENGTH_SHORT).show();
        } else {
            finish();
        }
    }
}
