package com.apps.resepmasakanoffline.rest;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

public class LoadData {

    public String FromAssets(Context mContext, String file) {
        String json = null;
        try {
            InputStream inputStream = mContext.getAssets().open("data/"+file+".json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    public void loadImage(Context mContext, ImageView imageView, String imageName) {
        try {
            InputStream inputstream = mContext.getAssets().open("images/" + imageName);
            Drawable drawable = Drawable.createFromStream(inputstream, null);
            imageView.setImageDrawable(drawable);
        } catch (Exception ex) {
            Log.e("Load Image Assets", ex.getMessage());
        }
    }

}
