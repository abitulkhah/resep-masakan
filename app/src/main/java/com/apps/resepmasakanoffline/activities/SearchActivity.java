package com.apps.resepmasakanoffline.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.adapters.ResepBigAdapter;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.apps.resepmasakanoffline.models.ResepList;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private RecyclerView recyclerView;
    private ResepBigAdapter resepBigAdapter;
    private Spinner searchCategorySP;
    private Button searchButton;
    private String searchValue = "default";
    private String categoryName = "default";
    private SearchView searchView;
    private FloatingActionButton arrowUpSearchNews;

    private List<Object> resepListTemp = new ArrayList<>();
    private List<Object> resepList = new ArrayList<>();
    private int page = -1;
    private int size = 10;
    private boolean resetData = false;
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_big);
        searchCategorySP = (Spinner) findViewById(R.id.search_category_sp);
        searchButton = (Button) findViewById(R.id.search_resep_button);
        arrowUpSearchNews = (FloatingActionButton) findViewById(R.id.arrow_up_searchresep);
        mContext = getApplicationContext();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.resep_category, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        searchCategorySP.setAdapter(adapter);
        searchCategorySP.setOnItemSelectedListener(this);
        searchCategorySP.setVisibility(View.VISIBLE);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchView.getQuery().toString().equalsIgnoreCase(null) || searchView.getQuery().toString().equalsIgnoreCase("")){
                    searchValue = "default";
                }else{
                    searchValue = searchView.getQuery().toString();
                    resetData = true;
                }
                getData();
            }
        });

        arrowUpSearchNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });

    }

    public void getData(){

        page = page+1;

        page = page+1;
        Gson gson = new Gson();
        LoadData loadData = new LoadData();
        ResepList resepList = gson.fromJson(loadData.FromAssets(mContext, "resep"), ResepList.class);

        List<ResepItem> resepItems = new ArrayList<>();
        for (int i = 0; i < resepList.getItems().size(); i++) {
            if(categoryName =="default"){
                if(searchValue !="default" && resepList.getItems().get(i).getTitle().toLowerCase().contains(searchValue.toLowerCase())
                        || resepList.getItems().get(i).getDescription().toLowerCase().contains(searchValue.toLowerCase())){

                    resepItems.add(resepList.getItems().get(i));
                }else{
                    resepItems.add(resepList.getItems().get(i));
                }
            }else{
                if(searchValue !="default" && resepList.getItems().get(i).getTitle().toLowerCase().contains(searchValue.toLowerCase())
                        || resepList.getItems().get(i).getDescription().toLowerCase().contains(searchValue.toLowerCase())
                        || resepList.getItems().get(i).getCategory().getCategoryName().toLowerCase().contains(categoryName.toLowerCase())){

                    resepItems.add(resepList.getItems().get(i));

                }else if(resepList.getItems().get(i).getCategory().getCategoryName().toLowerCase().contains(categoryName.toLowerCase())){
                    resepItems.add(resepList.getItems().get(i));
                }
            }
        }

        for (int idx = 0; idx < resepItems.size(); idx++) {
            if(idx > 0 && idx%3==0){
                resepItems.add(resepItems.get(idx));
            }
        }

        resepBigAdapter = new ResepBigAdapter(mContext, resepItems);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepBigAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        if(page==0){
            recyclerView.addItemDecoration(dividerItemDecoration);
        }
        resepBigAdapter.notifyDataSetChanged();
        recyclerView.setFocusable(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
       super.onBackPressed();
       return true;
    }

    public boolean onCreateOptionsMenu( Menu menu) {

        getMenuInflater().inflate( R.menu.searchview_action_bar, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.requestFocus();
        searchView.setIconifiedByDefault(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query==null || query.toString()==""){
                    searchValue = "default";
                }else{
                    searchValue = query;
                }
                getData();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText==null || newText.toString()==""){
                    searchValue = "default";
                }else{
                    searchValue = newText;
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        resetData = true;
        categoryName = parent.getSelectedItemPosition() == 0 ? "default" : parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
