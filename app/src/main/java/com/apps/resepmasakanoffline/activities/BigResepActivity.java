package com.apps.resepmasakanoffline.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.adapters.ResepBigAdapter;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.apps.resepmasakanoffline.models.ResepList;
import com.apps.resepmasakanoffline.rest.LoadData;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class BigResepActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private RecyclerView recyclerView;
    private ResepBigAdapter resepBigAdapter;
    private Spinner bigResepSelectSP;
    private FloatingActionButton arrowUpBigResep;
    private List<Object> resepListTemp = new ArrayList<>();
    private List<Object> resepList = new ArrayList<>();

    private int page = -1;
    private int size = 10;
    private String categoryName = null;
    private long categoryId = 0;

    private SwipeRefreshLayout swipeContainer;
    // The number of native ads to load.
    public static final int NUMBER_OF_ADS = 3;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of native ads that have been successfully loaded.
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_resep);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mContext = getApplicationContext();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_big);
        bigResepSelectSP = (Spinner) findViewById(R.id.bigResepSelectSP);
        arrowUpBigResep = (FloatingActionButton) findViewById(R.id.arrow_up_bigresep);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.resep_category, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.select_dialog_item);
        bigResepSelectSP.setAdapter(adapter);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainerBigResep);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        arrowUpBigResep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });


        Intent intent = getIntent();
        categoryName = intent.getStringExtra("resepCategoryName");
        setTitle(categoryName);
        categoryName = intent.getStringExtra("resepCategoryName");
        categoryId = intent.getLongExtra("resepCategoryId", 0);
        getData();

    }

    public void getData(){

        page = page+1;
        Gson gson = new Gson();
        LoadData loadData = new LoadData();
        ResepList resepList = gson.fromJson(loadData.FromAssets(mContext, "resep"), ResepList.class);

        List<ResepItem> resepItems = new ArrayList<>();
        for (int i = 0; i < resepList.getItems().size(); i++) {
            if(resepList.getItems().get(i).getCategory().getId()==categoryId){
                resepItems.add(resepList.getItems().get(i));
            }
        }

        for (int idx = 0; idx < resepItems.size(); idx++) {
            if(idx > 0 && idx%3==0){
                resepItems.add(resepItems.get(idx));
            }
        }

        resepBigAdapter = new ResepBigAdapter(mContext, resepItems);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepBigAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        if(page==0){
            recyclerView.addItemDecoration(dividerItemDecoration);
        }
        resepBigAdapter.notifyDataSetChanged();
        recyclerView.setFocusable(true);
        swipeContainer.setRefreshing(false);

    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        setTitle("Makanan "+parent.getSelectedItem());
        categoryName = parent.getSelectedItem().toString();
        getData();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
