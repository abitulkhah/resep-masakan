package com.apps.resepmasakanoffline.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.config.Config;
import com.apps.resepmasakanoffline.rest.LoadData;

public class DetailResepActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView detailResepContent;
    private RecyclerView recyclerView;
    private TextView resepDetailTitle;
    private TextView creator;
    private TextView detailCategoryName;
    private TextView sourceLink;
    private ImageView imageResepDetail;
    private String resepTitle = null;
    private String imageUri = null;
    private long resepId = 0;
    private LoadData loadData = new LoadData();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_resep);

        resepDetailTitle =  (TextView) findViewById(R.id.detailResepTitle);
        creator =  (TextView) findViewById(R.id.creator);
        detailCategoryName =  (TextView) findViewById(R.id.detailCategoryName);
        imageResepDetail =  (ImageView) findViewById(R.id.detailImageResep);
        detailResepContent = (TextView) findViewById(R.id.detailResepContent);
        sourceLink = (TextView) findViewById(R.id.sourceLink);

        final Intent intent = getIntent();

        loadData.loadImage(getApplicationContext(), imageResepDetail, intent.getStringExtra("image"));
        resepDetailTitle.setText(intent.getStringExtra("resepTitle"));
        detailCategoryName.setText(intent.getStringExtra("categoryName"));
        sourceLink.setText(intent.getStringExtra("url"));
        resepId = intent.getLongExtra("resepId", 0);
        resepTitle = intent.getStringExtra("resepTitle").replaceAll("[ ,;]","-");
        
        String description = intent.getStringExtra("description");

        if(description!=null && description!="null"){
            detailResepContent.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                detailResepContent.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
            } else {
                detailResepContent.setText(Html.fromHtml(description));
            }

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.detail_resep_action_bar, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.resepDetailShare:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Resep Makanan Share");
                intent.putExtra(Intent.EXTRA_TEXT, Config.SHARE_NEWS);
                startActivity(Intent.createChooser(intent, "Share Link"));
                return true;
            case R.id.smallResize:
                detailResepContent.setTextSize(14.0f);
                return true;
            case R.id.mediumResize:
                detailResepContent.setTextSize(20.0f);
                return true;
            case R.id.bigResize:
                detailResepContent.setTextSize(24.0f);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void directPage(Class activityClass){
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ((TextView) view).setTextColor(Color.TRANSPARENT);
        ((TextView)view).setText(null);
        float newSize =  0.0f;

        if(position==0){
            newSize = 14.0f;
        }else if(position==1){
            newSize = 18.0f;
        }else{
            newSize = 24.0f;
        }

        detailResepContent.setTextSize(newSize);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
