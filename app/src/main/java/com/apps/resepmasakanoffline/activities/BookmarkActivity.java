package com.apps.resepmasakanoffline.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.resepmasakanoffline.R;
import com.apps.resepmasakanoffline.adapters.ResepSmallAdapter;
import com.apps.resepmasakanoffline.models.ResepItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

;

public class BookmarkActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ResepSmallAdapter resepSmallAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_bookmark);

        SharedPreferences sharedPreferences = this.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        String bookmarkData = sharedPreferences.getString("bookmarkList", "");

        Gson gson = new Gson();
        List<ResepItem> resepList;

        Type type = new TypeToken<List<ResepItem>>() {
        }.getType();

        resepList = gson.fromJson(bookmarkData, type);

        if(resepList == null){
            resepList = new ArrayList<>();
        }

        resepSmallAdapter = new ResepSmallAdapter(this,  new ArrayList<ResepItem>(resepList));

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resepSmallAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        resepSmallAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    
}
